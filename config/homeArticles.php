<?php

require_once 'conn.php';

$conn = connect('heroku_e648ba7d56c83e4');

$stm = $conn->prepare("SELECT * FROM articles WHERE deleted_at IS NULL;");

$stm->execute();

$result = $stm->fetchAll(PDO::FETCH_ASSOC);

echo json_encode($result);