<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>DEV Community Clone</title>
    <link rel="icon" href="img/devIcon.png">
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Hind+Siliguri:wght@400;500;700&display=swap">
    <link rel="stylesheet" href="css/indexBaseline.css">
    <link rel="stylesheet" href="css/indexHeader.css">
    <link rel="stylesheet" href="css/indexNav.css">
    <link rel="stylesheet" href="css/indexMain.css">
    <link rel="stylesheet" href="css/indexAside.css">
    <script defer src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.18.0/axios.min.js"></script>
    <script defer src="js/home/homeArticles.js"></script>
    <script defer src="js/home/home.js"></script>
    <script defer src="js/login/logout.js"></script>
</head>
<body>
    <div class="header">
        <div class="homeSearch">
            <i id="navOpen" class="fas fa-bars"></i>
            <div class="homeHeader">
                <img id="homeIcon" class="homeIcon" src="img/imgHome.png" alt="DEV Icon">
            </div>
            <div>
                <input type="search" class="search" placeholder="Search...">
            </div>
        </div>
        <div class="headerButtons">
            <i class="fas fa-search"></i>
            <a id="headerLogin" href="login.html" class="headerLogIn">Log in</a>
            <a id="headerCreateAccount" href="createAccount.html" class="headercreateAcc">Create account</a>
            <a id="headerWritePost" href="createPost.html" class="headerWritePost visibility">Write a post</a>
            <div id="headerIcons" class="headerIcons visibility">
                <div class="logedIcons">
                    <i class="far fa-comment-dots"></i>
                </div>
                <div class="logedIcons">
                    <i class="far fa-bell"></i>
                </div>
                <div class="userHeader">
                    <img id="userImg" class="imgAuthor">
                    <div id="userSettings" class="userSettings userSettingsOpacity visibility">
                        <div id="userData" class="userData">
                            <p id="userHeaderName">Name</p>
                            <p id="userHeaderUsername" class="settingsUsername">@Username</p>
                        </div>
                        <div class="sessionOptions">
                            <a class="userSettingsLinks" href="dashboard.html">Dashboard</a>
                            <a class="userSettingsLinks" href="createPost.html">Write a post</a>
                            <p class="userSettingsLinks">Reading List</p>
                            <p class="userSettingsLinks">Settings</p>
                        </div>
                        <div id="logout" class="signOut">
                            <p>Sign Out</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div id="navVisibility" class="responsiveNavBg navVisibility">
            <div class="nav">
                <p class="responsiveNavTitle">Dev Community Clone</p>
                <i id="navClose" class="fas fa-times"></i>
                <hr class="responsiveNavHr">
                <div class="navItems">
                    <div id="navSignIn" class="signIn">
                        <i class="icon fas fa-hands-helping"></i>
                        <a href="login.html" class="SingInText"><h4>Sign In/Up</h4></a>
                    </div>
                    <div id="navHome" class="loginChanges visibility">
                        <i class="icon fas fa-home"></i>
                        <p class="navText">Home</p>
                    </div>
                </div>
                <div class="navItems listings">
                    <i class="icon fas fa-scroll"></i>
                    <p class="navText">Listings</p>
                </div>
                <div class="navItems podcast">
                    <i class="icon fas fa-microphone-alt"></i>
                    <p class="navText">Podcasts</p>
                </div>
                <div class="navItems videos">
                    <i class="icon fas fa-video"></i>
                    <p class="navText">Videos</p>
                </div>
                <div class="navItems tag">
                    <i class="icon fas fa-tag"></i>
                    <p class="navText">Tags</p>
                </div>
                <div class="navItems more">
                    <p class="more">More...</p>
                </div>
                <div class="popularTags">
                    <div class="navTagsTitle">
                        <h4 id="navTags" class="popularTitle">Popular Tags</h4>
                        <i id="navCogIcon" class="fas fa-cog visibility"></i>
                    </div>
                    <div id="navItemTags" class="populars">
                        <p class="popularItems">#javascript</p>
                        <p class="popularItems">#webdev</p>
                        <p class="popularItems">#beginners</p>
                        <p class="popularItems">#react</p>
                        <p class="popularItems">#tutorial</p>
                        <p class="popularItems">#python</p>
                        <p class="popularItems">#programming</p>
                        <p class="popularItems">#html</p>
                        <p class="popularItems">#css</p>
                        <p class="popularItems">#node</p>
                        <p class="popularItems">#productivity</p>
                        <p class="popularItems">#github</p>
                    </div>
                </div>
                <div class="publicity">
                    <img class="publicityImg" src="img/publicityNav.jpeg" alt="publicityImg">
                    <p class="publicityText">Get Your Stickers!</p>
                </div>
            </div>
        </div>
        <div>
            <div class="mainHeader">
                <div class="posts"><h6>Posts</h6></div>
                <select class="mainResponsiveSelect">
                    <option value="Feed">Feed</option>
                    <option value="Week">Week</option>
                    <option value="Month">Month</option>
                    <option value="Year">Year</option>
                    <option value="Infinity">Infinity</option>
                    <option value="Latest">Latest</option>
                </select>
                <div class="priority">
                    <div class="priorityItems active">
                        <p>Feed</p>
                        <hr class="hr">
                    </div>
                    <p class="priorityItems">Week</p>
                    <p class="priorityItems">Month</p>
                    <p class="priorityItems">Year</p>
                    <p class="priorityItems">Infinity</p>
                    <p class="priorityItems">Latest</p>
                </div>
            </div>
            <div id="articlesContainer" class="mainContent">
                <i id="spinner" class="fas fa-spinner"></i>
            </div>
        </div>
        <div class="aside">
            <div class="dev">
                <img src="img/rainbowdev.jpg" class="rainbow" alt="Rainbow DEV Icon">
                <h2 class="devTitle"><a href="#" class="outstandingText">DEV</a> is a community of 506,100 amazing developers</h2>
                <p class="devText">We're a place where coders share, stay up-to-date and grow their careers.</p>
                <div class="asideButtons">
                    <a href="#" class="asideCreate">Create new account</a>
                    <a href="#" class="asideLogin">Log in</a>
                </div>
            </div>
            <div class="asideListings">
                <div class="seeListings">
                    <h3 class="listingsTitle">Listings</h3>
                    <p class="seeAll">See all</p>
                </div>
                <div class="list">
                    <p class="asideText">Chat with Honeypot about you job hun experience</p>
                    <p class="asideTag">misc</p>
                </div>
                <div class="list">
                    <p class="asideText">Looking For Mentor</p>
                    <p class="asideTag">mentees</p>
                </div>
                <div class="list">
                    <p class="asideText">[ebook/PDF] The New Era of Developer Experience</p>
                    <p class="asideTag">education</p>
                </div>
                <div class="list">
                    <p class="asideText">Software Enginner - Senior QA</p>
                    <p class="asideTag">collabs</p>
                </div>
                <div class="list">
                    <p class="asideText">Courier: One API to notify users over email, Slack, SMS, push, Whatsapp & more</p>
                    <p class="asideTag">products</p>
                </div>
                <div class="list createList">Create a Listing</div>
            </div>
            <div class="news">
                <h3 class="newTitle">#news</h3>
                <div class="list">
                    <p class="asideText">Apple to Reduce App Store Commissions for Small Busesses in 2021</p>
                    <p class="asideTag">6 comments</p>
                </div>
                <div class="list">
                    <p class="asideText">Advent Of Code 2020</p>
                    <p class="asideTag">1 comment</p>
                </div>
                <div class="list">
                    <p class="asideText">Whats new in Mozilla Firefox 83.0?</p>
                    <p class="tagsNew">New</p>
                </div>
                <div class="list">
                    <p class="asideText">Super Powerful File Organizer</p>
                    <p class="tagsNew">New</p>
                </div>
            </div>
        </div>
    </div>
</body>
</html>