# Dev Community Clone 💻

---

My first dynamic project with the aim of using it in my portfolio.
This project is a clone of a developers blog, in which you can view, create, update and delete articles and create, update and delete users. It is also responsive.

## Watch the project on Heroku 👀

---

### [Heroku: Dev Clone](https://dev-clone-ml.herokuapp.com/).

## What does the project look like? 🎨

---

1. Index.
   ![index](https://bytebucket.org/MicaLoustou/dev-clone/raw/70e519bf0a982b5de4d316ca146434543dacb9c8/readmeImg/indexImage.png)
2. Login.
   ![login](https://bytebucket.org/MicaLoustou/dev-clone/raw/70e519bf0a982b5de4d316ca146434543dacb9c8/readmeImg/loginImage.png)
3. Profile.
   ![profile](https://bytebucket.org/MicaLoustou/dev-clone/raw/70e519bf0a982b5de4d316ca146434543dacb9c8/readmeImg/profileImage.png)
4. Dashboard.
   ![dashboard](https://bytebucket.org/MicaLoustou/dev-clone/raw/70e519bf0a982b5de4d316ca146434543dacb9c8/readmeImg/dashboardImage.png)
5. Create Post.
   ![createPost](https://bytebucket.org/MicaLoustou/dev-clone/raw/70e519bf0a982b5de4d316ca146434543dacb9c8/readmeImg/createPostImage.png)
6. Article.
   ![article](https://bytebucket.org/MicaLoustou/dev-clone/raw/70e519bf0a982b5de4d316ca146434543dacb9c8/readmeImg/articleImage.png)

## Tools 🔧

---

For build this proyect i used:

- HTML 5.
- CSS 3.
- JavaScript ES6.
- Axios
- SQL.
- PHP.
- Bootstrap.
- APIs REST like - Cloudinary.

## To run this proyect 🏃‍♂️

---

1. Install [Xampp](https://www.apachefriends.org/es/download.html).
2. clone this repository inside htdocs folder in xampp folder.
3. Run xampp with Apache.
   ![xampp](https://bytebucket.org/MicaLoustou/dev-clone/raw/7c0428e54cf6d41af747df8abb0b39d1d0af5e49/readmeIMG/xampp.png)
4. Open the project in this link [Dev-Clone](http://localhost/Dev-clone/).

---

⌨️ with ❤️ by [Mica Loustou](https://bitbucket.org/MicaLoustou/) 😊
