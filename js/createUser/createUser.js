if (localStorage.getItem('userSession')) {
    window.location.replace(window.location.origin + '/profile.html');
};

const createUserForm = document.getElementById('createUserForm');
const email = document.getElementById('email');
const username = document.getElementById('username');
const password = document.getElementById('password');
const confirmPassword = document.getElementById('confirmPassword');

createUserForm.addEventListener('submit', async (e) => {
    e.preventDefault();
    
    if (password.value === confirmPassword.value) {
        const formData = new FormData(createUserForm);

        const res = await axios.post( 'config/createUser.php', formData, {
            headers: {
                'Content-Type': 'multipart/form-data'
            }
        });
        
        if (res.data == 'ok') {
            const userEmail = new FormData()
            userEmail.append('email', email.value);

            const resUser = await axios.post('config/login.php', userEmail,{
                headers:{
                    'Content-Type' : 'multipart/form-data'
                }
            });
        
            const userDB = resUser.data[0];

            if (userDB.email) {
                userDB.password = null;
                localStorage.setItem('userSession', JSON.stringify(userDB));
            }
            
            window.location.replace(window.location.origin + '/index.php');
        } else {
            alert(res.data);
        };
    } else {
        alert('Passwords do not match');
    };
});