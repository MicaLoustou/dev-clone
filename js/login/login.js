const loginForm = document.getElementById('loginForm');

if (localStorage.getItem('userSession')) {
    window.location.replace(window.location.origin + '/profile.html');

}else {
loginForm.addEventListener('submit', async (e) => {
    e.preventDefault();

    const formData = new FormData(loginForm);
    const formPass = formData.get('pass');
    const res = await axios.post('config/login.php', formData,{
            headers:{
                'Content-Type' : 'multipart/form-data'
            }
        });
    
    const userDB = res.data[0];

    if(userDB.password === formPass) {
        userDB.password = null;
        localStorage.setItem('userSession', JSON.stringify(userDB));
        
        window.location.replace(window.location.origin + '/index.php');
    }else {
        alert('Email or password are incorrect');
    }
});
};