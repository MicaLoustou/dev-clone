const logout = document.getElementById('logout');

if(localStorage.getItem('userSession')) {
    logout.addEventListener('click', () => {
        localStorage.removeItem('userSession');
        window.location.assign(window.location.href)
    });
};