const articlesContainer = document.getElementById('articlesContainer');
const spinner = document.getElementById('spinner');
const fragment = document.createDocumentFragment();

window.addEventListener('load', async () => {
    const article = await axios('config/homeArticles.php')

    for(let i = 0; i < article.data.length; i++) {
        const authorID =  article.data[i].user_id;
        const formData = new FormData();
        formData.append('id', authorID);
        
        const authorAxios = await axios.post('config/header.php', formData, {
            headers: {
                'Content-Type' : 'multipart/form-data'
            }
        });
        const author = authorAxios.data[0];
        
        const articleElement = document.createElement('article');
        articleElement.setAttribute( 'data-article-id', `${article.data[i].id}` );
        articleElement.classList.add('article');

        if (article.data[i].cover) {
            const coverArticleContainer = document.createElement('div')
            coverArticleContainer.classList.add('coverArticleContainer');
            const imgArticle = document.createElement('img');
            imgArticle.classList.add('imgArticle');
            imgArticle.src = article.data[i].cover;
            coverArticleContainer.appendChild(imgArticle);
            articleElement.appendChild(coverArticleContainer);
        };

        const headerArticle = document.createElement('div');
        headerArticle.classList.add('headerArticle');
        articleElement.appendChild(headerArticle);

        const imgAuthor = document.createElement('img');
        imgAuthor.classList.add('imgAuthor');
        author.image ? imgAuthor.src = author.image : null;
        headerArticle.appendChild(imgAuthor);

        const authorDataArticle = document.createElement('div');
        headerArticle.appendChild(authorDataArticle);

        const nameAuthor = document.createElement('p');
        nameAuthor.classList.add('nameAuthor');
        nameAuthor.textContent = author.name;
        authorDataArticle.appendChild(nameAuthor);
        
        const publicationDate = document.createElement('p');
        publicationDate.classList.add('publicationDate');
        publicationDate.textContent = article.data[i].created_at;
        authorDataArticle.appendChild(publicationDate);

        const bodyArticle = document.createElement('div');
        bodyArticle.classList.add('bodyArticle');
        articleElement.appendChild(bodyArticle);

        const titleArticle = document.createElement('h1');
        titleArticle.classList.add('titleArticle');
        titleArticle.textContent = article.data[i].title;
        bodyArticle.appendChild(titleArticle);

        const hashtags = document.createElement('div');
        hashtags.classList.add('hashtags');
        bodyArticle.appendChild(hashtags);

        const hashtag = document.createElement('p');
        hashtag.classList.add('hashtag');
        hashtag.textContent = '#';
        hashtags.appendChild(hashtag);

        const hashtagSpan = document.createElement('span');
        hashtagSpan.textContent = article.data[i].tags;
        hashtag.appendChild(hashtagSpan);
        
        const footerArticle = document.createElement('div');
        footerArticle.classList.add('footerArticle');
        articleElement.appendChild(footerArticle);

        const interactions = document.createElement('div');
        interactions.classList.add('interactions');
        footerArticle.appendChild(interactions);

        const reactions = document.createElement('div');
        reactions.classList.add('reactions');
        interactions.appendChild(reactions);

        const heart = document.createElement('i');
        heart.classList.add('far', 'fa-heart');
        reactions.appendChild(heart);

        const interactionText = document.createElement('p');
        interactionText.classList.add('interactionText');
        interactionText.textContent = Math.floor(Math.random() * (100 - 0)) + 0;
        reactions.appendChild(interactionText);

        const responsiveinteractionText = document.createElement('span');
        responsiveinteractionText.classList.add('responsiveinteractionText');
        responsiveinteractionText.textContent = 'reactions';
        interactionText.appendChild(responsiveinteractionText);

        const comments = document.createElement('div');
        comments.classList.add('comments');
        interactions.appendChild(comments);

        const commentIcon = document.createElement('i');
        commentIcon.classList.add('far', 'fa-comment');
        comments.appendChild(commentIcon);

        const interactionText2 = document.createElement('p');
        interactionText2.classList.add('interactionText');
        interactionText2.textContent = Math.floor(Math.random() * (100 - 0)) + 0;
        comments.appendChild(interactionText2);

        const responsiveinteractionText2 = document.createElement('span');
        responsiveinteractionText2.classList.add('responsiveinteractionText');
        responsiveinteractionText2.textContent = 'comments';
        interactionText2.appendChild(responsiveinteractionText2);

        const interactionSave = document.createElement('div');
        interactionSave.classList.add('interactionSave');
        footerArticle.appendChild(interactionSave);

        const readTime = document.createElement('p');
        readTime.classList.add('readTime');
        readTime.textContent = Math.floor(Math.random() * (11 - 1)) + 1 + ' min read';
        interactionSave.appendChild(readTime);

        const save = document.createElement('p');
        save.classList.add('save');
        save.textContent = 'Save';
        interactionSave.appendChild(save);

        fragment.append(articleElement)
    }

    articlesContainer.removeChild(spinner);
    articlesContainer.appendChild(fragment);
});

/* Fragment Template:

<article class="article">
    <div class="coverArticleContainer">
        <img class="imgArticle" src="img/imgarticle.jpeg" alt="The image of this article is not available">
    </div>
    <div class="headerArticle">
        <img class="imgAuthor">
        <div>
            <p class="nameAuthor">Simon Holdorf</p>
            <p class="publicationDate">Dic 22</p>
        </div>
    </div>
    <div class="bodyArticle">
        <h1 class="titleArticle">Article Title</h1>
        <div class="hashtags">
            <p class="hashtag">#<span>hashtag</span></p>
            <p class="hashtag">#<span>hashtag</span></p>
            <p class="hashtag">#<span>hashtag</span></p>
        </div>
    </div>
    <div class="footerArticle">
        <div class="interactions">
            <div class="reactions">
                <i class="far fa-heart"></i>
                <p class="interactionText">0 <span class="responsiveinteractionText">reactions</span></p>
            </div>
            <div class="comments">
                <i class="far fa-comment"></i>
                <p class="interactionText">0 <span class="responsiveinteractionText">comments</span></p>
            </div>
        </div>
        <div class="interactionSave">
            <p class="readTime">reading time</p>
            <p class="save">Save</p>
        </div>
    </div>
</article>

*/

articlesContainer.addEventListener('click', (e) => {
    for (let i = 0; i < e.path.length; i++) {
        if (e.path[i].nodeName === 'ARTICLE') {
            const articleID = e.path[i].dataset.articleId;
            localStorage.setItem('articleID', articleID);
            window.location.assign(window.location.origin + '/article.html');
        };
    };
});