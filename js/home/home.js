const homeIcon = document.getElementById('homeIcon');
const login = document.getElementById('headerLogin');
const createAccount = document.getElementById('headerCreateAccount');
const headerWritePost = document.getElementById('headerWritePost');
const headerIcons = document.getElementById('headerIcons');
const userImg = document.getElementById('userImg');
const userData = document.getElementById('userData');
const navHome = document.getElementById('navHome');
const navTags = document.getElementById('navTags');
const navItemTags = document.getElementById('navItemTags');
const navCogIcon = document.getElementById('navCogIcon');
const userHeader = document.getElementById('userHeader');
const userSettings = document.getElementById('userSettings');
const navSignIn = document.getElementById('navSignIn');
const navVisibility = document.getElementById('navVisibility');
const navClose = document.getElementById('navClose');
const navOpen = document.getElementById('navOpen');
const userHeaderUsername = document.getElementById('userHeaderUsername');
const userHeaderName = document.getElementById('userHeaderName');
const userSession = JSON.parse(localStorage.getItem('userSession'));

const connUserDB = async (userID) => {
    const formData = new FormData();
    formData.append('id', userID);
    const res = await axios.post( 'config/header.php', formData, {
        headers: {
            'Content-Type': 'multipart/form-data'
        }
    });
    return res.data[0];
};

window.addEventListener('load', async () => {
    if (userSession) {
        const resData = await connUserDB(userSession.id);
        resData.image ? userImg.src = resData.image : null;
        userHeaderName.textContent = resData.name
        userHeaderUsername.textContent = resData.username

        navSignIn.classList.add('visibility');
        navHome.classList.remove('visibility');
        login.classList.add('visibility');
        createAccount.classList.add('visibility');
        headerWritePost.classList.remove('visibility')
        headerIcons.classList.remove('visibility');
        navTags.textContent = 'My Tags'
        navItemTags.classList.add('visibility')
        navCogIcon.classList.remove('visibility')
    };
});

userImg.addEventListener('click', () => {
    window.location.replace(window.location.origin + '/profile.html');
});

userData.addEventListener('click', () => {
    userImg.click()
});

navOpen.addEventListener('click', () => {
    navVisibility.classList.remove('navVisibility');
});

navClose.addEventListener('click', () => {
    navVisibility.classList.add('navVisibility');
});