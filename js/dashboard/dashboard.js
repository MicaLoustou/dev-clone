const userImg = document.getElementById('userImg');
const userHeaderUsername = document.getElementById('userHeaderUsername');
const userHeaderName = document.getElementById('userHeaderName');
const userSession = JSON.parse(localStorage.getItem('userSession'));
const cardWithoutArticles = document.getElementById('cardWithoutArticles');
const cardsList = document.getElementById('dashboardCards');
const numItems = document.getElementById('numItems');

const connUserDB = async (userID) => {
    const formData = new FormData();
    formData.append('id', userID);
    const res = await axios.post( 'config/header.php', formData, {
        headers: {
            'Content-Type': 'multipart/form-data'
        }
    });
    return res.data[0];
};

window.addEventListener('load', async () => {
    if (userSession) {
        const resData = await connUserDB(userSession.id);
        resData.image ? userImg.src = resData.image : null;
        userHeaderName.textContent = resData.name
        userHeaderUsername.textContent = resData.username
    } else {
        window.location.replace(window.location.origin + '/login.html');
    };

    const formData = new FormData();
    formData.append('id', userSession.id);


    const articlesData = await axios.post('config/userArticles.php', formData, {
        headers: {
            'Content-Type' : 'multipart/form-data'
        }
    });

    
    if(articlesData.data.length > 0) {
        cardWithoutArticles.classList.add('visibility');

        numItems.textContent = articlesData.data.length;

        const fragment = document.createDocumentFragment();
        
        for(let i = 0; i < articlesData.data.length; i++) {

            const dashboardCards = document.createElement('div');
            dashboardCards.classList.add('dashboardCards');
            
            const titleCards = document.createElement('div');
            titleCards.classList.add('titleCards')
            dashboardCards.appendChild(titleCards);

            const title = document.createElement('p');
            title.classList.add('title');
            title.id = "articleTitle";
            title.setAttribute( 'data-article-id', `${articlesData.data[i].id}` );
            title.textContent = articlesData.data[i].title;
            titleCards.appendChild(title);

            const publishDate = document.createElement('p');
            publishDate.classList.add('publishDate');
            publishDate.textContent = articlesData.data[i].created_at;
            titleCards.appendChild(publishDate);
            
            
            const iconsCards = document.createElement('div');
            iconsCards.classList.add('iconsCards');

            const heart = document.createElement('i');
            heart.classList.add('far','fa-heart');
            heart.textContent = Math.floor(Math.random() * (100 - 0)) + 0;
            iconsCards.appendChild(heart);

            const comment = document.createElement('i');
            comment.classList.add('far','fa-comment');
            comment.textContent = Math.floor(Math.random() * (100 - 0)) + 0;
            iconsCards.appendChild(comment);

            const eye = document.createElement('i');
            eye.classList.add('far','fa-eye');
            eye.textContent = Math.floor(Math.random() * (100 - 0)) + 0;
            iconsCards.appendChild(eye);

            dashboardCards.appendChild(iconsCards);

            const optionsCards = document.createElement('div');
            optionsCards.classList.add('optionsCards');

            const edit = document.createElement('p');
            edit.setAttribute( 'data-article-id', `${articlesData.data[i].id}` );
            edit.id = 'editButton';
            edit.textContent = 'Edit';
            optionsCards.appendChild(edit);

            const deleteButton = document.createElement('p');
            deleteButton.setAttribute( 'data-article-id', `${articlesData.data[i].id}` );
            deleteButton.id = 'deleteButton';
            deleteButton.textContent = 'Delete';
            optionsCards.appendChild(deleteButton);

            const ellipsis = document.createElement('i');
            ellipsis.classList.add('fas','fa-ellipsis-h');
            optionsCards.appendChild(ellipsis);

            dashboardCards.appendChild(optionsCards);
            
            fragment.appendChild(dashboardCards);
        };

        cardsList.appendChild(fragment);

        /* Fragment Template:

        <div class="titleCards">
            <a href="#" class="title">TITLE</a>
            <p class="publishDate">Published: 25 nov.</p>
        </div>
        <div class="iconsCards">
            <i class="far fa-heart"> 200</i>
            <i class="far fa-comment"> 62</i>
            <i class="far fa-eye"> 340</i>
        </div>
        <div class="optionsCards">
            <p>Manage</p>
            <p>Edit</p>
            <i class="fas fa-ellipsis-h"></i>
        </div>

        */
    };
});

cardsList.addEventListener('click', async (e) => {
    if(e.toElement.id === 'deleteButton') {
        if (confirm('Do you really want to delete the article?')) {
            const formData = new FormData();
            formData.append('article_id', e.toElement.dataset.articleId);

            const res = await axios.post( 'config/deleteArticle.php', formData, {
                headers: {
                    'Content-Type' : 'multipart/form-data'
                }
            });
            res.statusText == 'OK' ? window.location.reload() : alert('error when deleting your article');
        }
    } else if (e.toElement.id === 'editButton') {
        localStorage.setItem('editedArticle', e.toElement.dataset.articleId);
        window.location.replace(window.location.origin + '/editArticle.html');
    } else if (e.toElement.id === 'articleTitle') {
        localStorage.setItem('articleID', e.toElement.dataset.articleId);
        window.location.replace(window.location.origin + '/article.html');
    };
});