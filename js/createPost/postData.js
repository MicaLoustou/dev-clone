const fileInput = document.getElementById('file');
const coverLoaded = document.getElementById('coverLoaded');
const uploadCover = document.getElementById('uploadCover');
const coverImagePreview = document.getElementById('coverImagePreview');
const coverFile = document.getElementById('coverFile');
const coverRemove = document.getElementById('coverRemove');
const uploadImg = document.getElementById('uploadImg');
const uploadText = document.getElementById('uploadText');
const postTitle = document.getElementById('newTitle');
const postTags = document.getElementById('addTags');
const postText = document.getElementById('writePost');
const publishButton = document.getElementById('publishButton');
const revertChanges = document.getElementById('revertChanges');
const userID = JSON.parse(localStorage.getItem('userSession'));

let postImage = null;
let coverImage = null;
let coverPreview = new FileReader();

const cloudinary_url = 'https://api.cloudinary.com/v1_1/micaloustou/image/upload';
const cloudinary_pass = 'ml_default';

uploadCover.addEventListener('click', () => {
    coverFile.click();
});

coverFile.addEventListener('change', async (e) => {
    const file = e.target.files[0];
    
    uploadCover.textContent = 'Loading...';

    coverPreview.readAsDataURL(file);

    const formData = new FormData();
    formData.append('file', file);
    formData.append('upload_preset', cloudinary_pass);

    // Send to cloudinary
    const res = await axios.post( cloudinary_url, formData, {
        headers: {
            'Content-Type': 'multipart/form-data'
        }
    });
    coverImage = res.data.secure_url;

    if (coverImage != null) {
        coverImagePreview.src = coverPreview.result;
        coverLoaded.classList.remove('coverLoadedHidden');
        coverLoaded.classList.add('coverLoaded');
        coverRemove.classList.remove('coverRemoveHidden');
        coverRemove.classList.add('coverRemove');
        uploadCover.textContent = 'Change';
    }
});

coverRemove.addEventListener('click', () => {
    coverImage = null;
    coverLoaded.classList.remove('coverLoaded');
    coverLoaded.classList.add('coverLoadedHidden');
    coverRemove.classList.remove('coverRemove');
    coverRemove.classList.add('coverRemoveHidden');
    uploadCover.textContent = 'Add a cover image';
    coverImagePreview.src = '';
});

uploadImg.addEventListener('click', () => {
    fileInput.click();
});

fileInput.addEventListener('change', async (e) => {
    const file = e.target.files[0];

    uploadText.textContent = file.name;

    const formData = new FormData();
    formData.append('file', file);
    formData.append('upload_preset', cloudinary_pass);

    // Send to cloudinary
    const res = await axios.post( cloudinary_url, formData, {
        headers: {
            'Content-Type': 'multipart/form-data'
        }
    });
    postImage = res.data.secure_url;
});

revertChanges.addEventListener('click', () => {
    coverImage = null;
    postImage = null;
    coverLoaded.classList.remove('coverLoaded');
    coverLoaded.classList.add('coverLoadedHidden');
    coverRemove.classList.remove('coverRemove');
    coverRemove.classList.add('coverRemoveHidden');
    uploadCover.textContent = 'Add a cover image';
    coverImagePreview.src = '';
    postTitle.value = '';
    postTags.value = '';
    postText.value = '';
});

publishButton.addEventListener('click', async () => {
    const formData = new FormData();
    if (postTitle.value.length >= 1 && postTags.value.length >= 1 && postText.value.length >= 1) {
        formData.append('coverImg', coverImage);
        formData.append('postImg', postImage);
        formData.append('title', postTitle.value);
        formData.append('tags', postTags.value);
        formData.append('text', postText.value);
        formData.append('user_id', userID.id);

        const res = await axios.post( 'config/createPost.php', formData, {
            headers: {
                'Content-Type': 'multipart/form-data'
            }
        });
        
        if (res.data == 'ok') {
            window.location.replace(window.location.origin + '/index.php');
        } else {
            alert(res.data);
        };

    } else {
        alert('You most complete the required fields: Title, Tags and Content');
    }
});