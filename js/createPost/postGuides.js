if (!localStorage.getItem('userSession')) {
    window.location.replace(window.location.origin + '/login.html');
};

const newTitle = document.getElementById('newTitle');
const addTags = document.getElementById('addTags');
const writePost = document.getElementById('writePost');
const firstGuide = document.getElementById('firstGuide');
const secondGuide = document.getElementById('secondGuide');
const thirdGuide = document.getElementById('thirdGuide');

newTitle.addEventListener('click', (e) => {
    firstGuide.classList.add('visibility');
    secondGuide.classList.remove('visibility');
    thirdGuide.classList.remove('visibility');

});

addTags.addEventListener('click', (e) => {
    firstGuide.classList.remove('visibility');
    secondGuide.classList.add('visibility');
    thirdGuide.classList.remove('visibility');
});

writePost.addEventListener('click', (e) => {
    firstGuide.classList.remove('visibility');
    secondGuide.classList.remove('visibility');
    thirdGuide.classList.add('visibility');
});