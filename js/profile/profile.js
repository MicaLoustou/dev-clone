const userImg = document.getElementById('userImg');
const profileUserImg = document.getElementById('profileUserImg');
const profileName = document.getElementById('profileName');
const profileBiography = document.getElementById('profileBiography');
const profileDate = document.getElementById('profileDate');
const userHeaderUsername = document.getElementById('userHeaderUsername');
const userHeaderName = document.getElementById('userHeaderName');
const editButton = document.getElementById('editButton');
const modal = document.getElementById('modal');
const main = document.getElementById('main');
const closeButton = document.getElementById('closeButton');
const modalInputName = document.getElementById('modalInputName');
const modalInputUsername = document.getElementById('modalInputUsername');
const modalTextarea = document.getElementById('modalTextarea');
const image = document.getElementById('image');
const file = document.getElementById('file');
const updateButton = document.getElementById('updateButton');
const deleteButton = document.getElementById('deleteButton');
const userSession = JSON.parse(localStorage.getItem('userSession'));

let updatedFile = null;
const cloudinary_url = 'https://api.cloudinary.com/v1_1/micaloustou/image/upload';
const cloudinary_pass = 'ml_default';

const connUserDB = async (userID) => {
    const formData = new FormData();
    formData.append('id', userID);
    const res = await axios.post( 'config/header.php', formData, {
        headers: {
            'Content-Type': 'multipart/form-data'
        }
    });
    return res.data[0];
};

window.addEventListener('load', async () => {
    if (userSession) {
        const resData = await connUserDB(userSession.id);
        if (resData.image) {
            userImg.src = resData.image;
            profileUserImg.src = resData.image;
            image.src = resData.image;
            updatedFile = resData.image;
        }
        userHeaderName.textContent = resData.name;
        userHeaderUsername.textContent = resData.username;
        profileName.textContent = resData.name;
        profileBiography.textContent = resData.biography;
        profileDate.textContent = `Joined on ${resData.created_at}`;
        modalInputName.value = resData.name;
        modalInputUsername.value = resData.username;
        modalTextarea.value = resData.biography;

    } else {
        window.location.replace(window.location.origin + '/login.html');
    };
});

editButton.addEventListener('click', () => {
    modal.classList.remove('visibility');
    main.classList.add('visibility');
});

file.addEventListener('change', (e) => {
    const formData = new FormData();
    const updatedImagePreview = new FileReader();
    updatedFile = e.target.files[0];

    formData.append('',updatedFile);
    updatedImagePreview.readAsDataURL(updatedFile);

    updatedImagePreview.addEventListener('load', (e) => {
        image.src = e.target.result;
    });

})

closeButton.addEventListener('click', () => {
    main.classList.remove('visibility');
    modal.classList.add('visibility');
});

updateButton.addEventListener('click', async () => {
    if (typeof updatedFile == 'object') {
        const formData = new FormData();
        formData.append('file', updatedFile);
        formData.append('upload_preset', cloudinary_pass);
        const updateImage = await axios.post( cloudinary_url, formData, {
            headers: {
                'Content-Type': 'multipart/form-data'
            }
        });
        updatedFile = updateImage.data.secure_url;
    }

    const updatedFormData = new FormData();
    updatedFormData.append('id', userSession.id);
    updatedFormData.append('image', updatedFile);
    updatedFormData.append('name', modalInputName.value);
    updatedFormData.append('username', modalInputUsername.value);
    updatedFormData.append('biography', modalTextarea.value);

    const updateUser = await axios.post( 'config/editUser.php', updatedFormData, {
        headers: {
            'Content-Type': 'multipart/form-data'
        }
    });

    updateUser.data == 'ok' ? window.location.reload() : alert(`${updateUser.data}`);
});

deleteButton.addEventListener('click', async () => {
    if (confirm('Do you really want to delete your user?')) {
        const formData = new FormData();
        formData.append('user_id', userSession.id);

        const deleteUser = await axios.post( 'config/deleteUser.php', formData, {
            headers: {
                'Content-Type': 'multipart/form-data'
            }
        });
        localStorage.removeItem('userSession');
        window.location.replace(window.location.origin + '/index.php')
    };
});

