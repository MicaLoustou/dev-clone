const editedArticle = localStorage.getItem('editedArticle');
const editCover = document.getElementById('coverImagePreview');
const editTitle = document.getElementById('newTitle');
const editTags = document.getElementById('addTags');
const image = document.getElementById('uploadImg');
const imgText = document.getElementById('uploadText');
const editText = document.getElementById('writePost');
const editCoverLoaded = document.getElementById('coverLoaded');
const editUploadCover = document.getElementById('uploadCover');
const editCoverRemove = document.getElementById('coverRemove');
const editCoverFile = document.getElementById('coverFile');
const fileInput = document.getElementById('file');
const editRevertChanges = document.getElementById('revertChanges');
const updateButton = document.getElementById('publishButton');

let editPostImage;
let editCoverImage;
let editedArticleID = null;
let fileReader = new FileReader();

const cloudinary_url = 'https://api.cloudinary.com/v1_1/micaloustou/image/upload';
const cloudinary_pass = 'ml_default';

window.addEventListener('load', async (e) => {
    const formData = new FormData();
    formData.append('id', editedArticle);

    const res = await axios.post( 'config/article.php', formData, {
        headers: {
            'Content-Type': 'multipart/form-data'
        }
    });

    const articleData = res.data[0];
    
    if (articleData.cover) {
        editCoverImage = articleData.cover
        editCover.src = articleData.cover;
        editCoverLoaded.classList.remove('coverLoadedHidden');
        editCoverLoaded.classList.add('coverLoaded');
        editCoverRemove.classList.remove('coverRemoveHidden');
        editCoverRemove.classList.add('coverRemove');
        editUploadCover.textContent = 'Change';
    } else { editCoverImage = null };

    editedArticleID = articleData.id;
    
    articleData.cover ? editPostImage = articleData.img : editPostImage = null;

    editTitle.value = articleData.title;

    editTags.value = articleData.tags;

    editText.value = articleData.text;

});

editUploadCover.addEventListener('click', () => {
    editCoverFile.click();
});

editCoverFile.addEventListener('change', async (e) => {
    const file = e.target.files[0];
    
    editUploadCover.textContent = 'Loading...';

    fileReader.readAsDataURL(file);

    const formData = new FormData();
    formData.append('file', file);
    formData.append('upload_preset', cloudinary_pass);
    
    const res = await axios.post( cloudinary_url, formData, {
        headers: {
            'Content-Type': 'multipart/form-data'
        }
    });
    editCoverImage = res.data.secure_url;

    if (editCoverImage != null) {
        editCover.src = fileReader.result;
        editCoverLoaded.classList.remove('coverLoadedHidden');
        editCoverLoaded.classList.add('coverLoaded');
        editCoverRemove.classList.remove('coverRemoveHidden');
        editCoverRemove.classList.add('coverRemove');
        editUploadCover.textContent = 'Change';
    };
});

editCoverRemove.addEventListener('click', () => {
    editCoverImage = null;
    editCoverLoaded.classList.remove('coverLoaded');
    editCoverLoaded.classList.add('coverLoadedHidden');
    editCoverRemove.classList.remove('coverRemove');
    editCoverRemove.classList.add('coverRemoveHidden');
    editUploadCover.textContent = 'Upload a cover image';
    editCover.src = '';
});

image.addEventListener('click', () => {
    fileInput.click();
});

fileInput.addEventListener('change', async (e) => {
    const file = e.target.files[0];

    imgText.textContent = file.name;

    const formData = new FormData();
    formData.append('file', file);
    formData.append('upload_preset', cloudinary_pass);

    // Send to cloudinary
    const res = await axios.post( cloudinary_url, formData, {
        headers: {
            'Content-Type': 'multipart/form-data'
        }
    });
    editPostImage = res.data.secure_url;
});

editRevertChanges.addEventListener('click', () => {
    editCoverImage = null;
    editPostImage = null;
    editCoverLoaded.classList.remove('coverLoaded');
    editCoverLoaded.classList.add('coverLoadedHidden');
    editCoverRemove.classList.remove('coverRemove');
    editCoverRemove.classList.add('coverRemoveHidden');
    editUploadCover.textContent = 'Add a cover image';
    editCover.src = '';
    editTitle.value = '';
    editTags.value = '';
    editText.value = '';
});

updateButton.addEventListener('click', async () => {
    const formData = new FormData();

    if (editTitle.value.length >= 1 && editTags.value.length >= 1 && editText.value.length >= 1) {
        formData.append('id', editedArticleID);
        formData.append('coverImg', editCoverImage);
        formData.append('postImg', editPostImage);
        formData.append('title', editTitle.value);
        formData.append('tags', editTags.value);
        formData.append('text', editText.value);

        const res = await axios.post( 'config/editArticle.php', formData, {
            headers: {
                'Content-Type': 'multipart/form-data'
            }
        });
        console.log(res);
        if (res.data == 'ok') {
            window.location.replace(window.location.origin + '/dashboard.html');
        } else {
            alert(res.data);
        };

    } else {
        alert('You most complete the required fields: Title, Tags and Content');
    }
});