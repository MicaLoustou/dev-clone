const homeIcon = document.getElementById('homeIcon');
const login = document.getElementById('headerLogin');
const createAccount = document.getElementById('headerCreateAccount');
const headerWritePost = document.getElementById('headerWritePost');
const headerIcons = document.getElementById('headerIcons');
const userImg = document.getElementById('userImg');
const userData = document.getElementById('userData');
const userHeader = document.getElementById('userHeader');
const userSettings = document.getElementById('userSettings');
const userHeaderUsername = document.getElementById('userHeaderUsername');
const userHeaderName = document.getElementById('userHeaderName');
const heartIcon = document.getElementById('heartIcon');
const bookmarkIcon = document.getElementById('bookmarkIcon');
const horseIcon = document.getElementById('horseIcon');
const title = document.getElementById('title');
const tags = document.getElementById('tags');
const authorImg = document.querySelectorAll('[id=authorImg]');
const nameAuthor = document.querySelectorAll('[id=nameAuthor]');
const publicationDate = document.getElementById('publicationDate');
const articleContent = document.getElementById('articleContent');
const readTime = document.getElementById('readTime');
const biography = document.getElementById('biography');
const moreArticlesDiv = document.getElementById('moreArticlesDiv');
const article = document.getElementById('article');
const userSession = JSON.parse(localStorage.getItem('userSession'));
const articleID = JSON.parse(localStorage.getItem('articleID'));

!articleID ? window.location.replace(window.location.origin + '/index.php') : null;

const connUserDB = async (userID) => {
    const formData = new FormData();
    formData.append('id', userID);
    const res = await axios.post( 'config/header.php', formData, {
        headers: {
            'Content-Type': 'multipart/form-data'
        }
    });
    return res.data[0];
};

window.addEventListener('load', async () => {
    if (userSession) {
        const resData = await connUserDB(userSession.id);
        resData.image ? userImg.src = resData.image : null;
        userHeaderName.textContent = resData.name;
        userHeaderUsername.textContent = resData.username;

        login.classList.add('visibility');
        createAccount.classList.add('visibility');
        headerWritePost.classList.remove('visibility');
        headerIcons.classList.remove('visibility');
    };

    const formData = new FormData();

    formData.append('id', articleID);

    const res = await axios.post('config/article.php', formData, {
        headers: {
            'Content-Type' : 'multipart/form-data'
        }
    });
    
    const articleData = res.data[0];

    if (articleData.cover) {
        const coverDiv = document.createDocumentFragment();

        const coverArticleContainer = document.createElement('div');
        coverArticleContainer.classList.add('coverArticleContainer');
        coverDiv.appendChild(coverArticleContainer);

        const coverImg = document.createElement('img');
        coverImg.classList.add('imgArticle');
        coverImg.src = articleData.cover;
        coverArticleContainer.appendChild(coverImg);

        article.insertBefore(coverDiv, article.firstChild);
    }

    title.textContent = articleData.title;

    tags.textContent = articleData.tags;
    
    articleContent.textContent = articleData.text;

    if (articleData.img) {
        const imgDiv = document.createDocumentFragment();

        const imgArticleContainer = document.createElement('div');
        imgArticleContainer.classList.add('coverArticleContainer');
        imgDiv.appendChild(imgArticleContainer);

        const articleImg = document.createElement('img');
        articleImg.classList.add('imgArticle');
        articleImg.src = articleData.img;
        imgArticleContainer.appendChild(articleImg);

        imgArticleContainer.style.borderRadius = '5px';

    articleContent.insertBefore(imgDiv, articleContent.lastChild);
    };

    publicationDate.textContent = articleData.created_at;


    readTime.textContent = Math.floor(Math.random() * (11 - 1)) + 1 + ' min read';

    const authorData = await connUserDB(articleData.user_id);

    nameAuthor.textContent = authorData.name;

    authorData.image ? authorImg.src = authorData.image : null;

    for(names of nameAuthor) names.textContent = authorData.name;

    if (authorData.image) {
        for (images of authorImg) {
            images.src = authorData.image;
        }
    } else { null };

    authorData.biography ? biography.textContent = authorData.biography : biography.textContent = '404 Biography not found';

    const userArticlesData = new FormData();

    userArticlesData.append('id', articleData.user_id);

    const moreArticles = await axios.post('config/userArticles.php', userArticlesData, {
        headers: {
            'Content-Type' : 'multipart/form-data'
        }
    });

    const fragment = document.createDocumentFragment();

    if (moreArticles.data.length > 0) {
        for (let i = 0; i < moreArticles.data.length; i++) {

            const divElement = document.createElement('div');
            divElement.classList.add('otherArticles');
            fragment.appendChild(divElement);

            const title = document.createElement('p');
            title.textContent = moreArticles.data[i].title;
            divElement.appendChild(title);

            const tags = document.createElement('p');
            tags.classList.add('otherArticlesTags');
            tags.innerHTML = `<span>#</span>${moreArticles.data[i].tags}`;
            divElement.appendChild(tags);

            moreArticlesDiv.appendChild(fragment)
        }

        moreArticlesDiv.lastChild.classList.add('lastOtherArticles')

        /* Fragment Template:

        <div class="otherArticles">
            <p>Article Name</p>
            <p class="otherArticlesTags"><span>#</span>hashtags</p>
        </div>
        <div class="otherArticles">
            <p>Article Name</p>
            <p class="otherArticlesTags"><span>#</span>hashtags</p>
        </div>
        <div class="otherArticles lastOtherArticles">
            <p>Article Name</p>
            <p class="otherArticlesTags"><span>#</span>hashtags</p>
        </div>
        
        */
    }
});

userImg.addEventListener('click', () => {
    window.location.replace(window.location.origin + '/profile.html');
});

userData.addEventListener('click', () => {
    userImg.click();
});

heartIcon.addEventListener('click', () => {
    heartIcon.classList.contains('heartActive') ? heartIcon.classList.remove('heartActive') : heartIcon.classList.add('heartActive');
});

horseIcon.addEventListener('click', () => {
    horseIcon.classList.contains('horseActive') ? horseIcon.classList.remove('horseActive') : horseIcon.classList.add('horseActive');
});

bookmarkIcon.addEventListener('click', () => {
    bookmarkIcon.classList.contains('bookmarkActive') ? bookmarkIcon.classList.remove('bookmarkActive') : bookmarkIcon.classList.add('bookmarkActive');
});